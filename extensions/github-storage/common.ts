export const GITHUB_STORAGE_PROVIDER_NAME = 'github-storage';
export const OPERATION_NEW_GITHUB_WORKSPACE =
  'operation::@bangle.io/github-storage:new-workspace';
export const OPERATION_UPDATE_GITHUB_TOKEN =
  'operation::@bangle.io/github-storage:update-github-token';
export const OPERATION_PUSH_GITHUB_CHANGES =
  'operation::@bangle.io/github-storage:push-github-changes';

export const OPERATION_SYNC_GITHUB_CHANGES =
  'operation::@bangle.io/github-storage:sync-github-changes';

export const OPERATION_PULL_GITHUB_CHANGES =
  'operation::@bangle.io/github-storage:OPERATION_PULL_GITHUB_CHANGES';

export const OPERATION_DISCARD_LOCAL_CHANGES =
  'operation::@bangle.io/github-storage:OPERATION_DISCARD_LOCAL_CHANGES';

export const DISCARD_LOCAL_CHANGES_DIALOG =
  'dialog::@bangle.io/github-storage:discard-local-changes';

export const NEW_GITHUB_WORKSPACE_TOKEN_DIALOG =
  'dialog::@bangle.io/github-storage:NEW_GITHUB_WORKSPACE_TOKEN_DIALOG';

export const NEW_GITHUB_WORKSPACE_REPO_PICKER_DIALOG =
  'dialog::@bangle.io/github-storage:NEW_GITHUB_WORKSPACE_REPO_PICKER_DIALOG';
